.. Ohmpi documentation master file, created by
   sphinx-quickstart on Tue Jun 30 20:22:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

OHMPI: Open source and open hardware resistivity-meter
======================================================



.. sidebar:: Summary

    :Release: |release|
    :Date: |today|
    :Date start: July 2016	
    :Authors: **Rémi CLEMENT, Nicolas FORQUET, Yannick FARGIER, Vivien DUBOIS, Hélène GUYARD, Olivier KAUFMANN, Guillaume BLANCHY, Arnaud WATLET**
    :Target: users, researchers and developers 
    :status: some mature, some in progress

.. topic:: OhmPi Document Center

    * OhmPi official documents
    * Release guidelines
    * General tutorials

.. image:: img/logo/ohmpi/LOGO_OHMPI.png
   :width: 200px
   :height: 150px
   :align: left

Contents: 

.. toctree:: 
   :maxdepth: 2 

   source_rst/Ohmpi
   source_rst/hardware
   source_rst/software/index
   source_rst/troubleshooting
   source_rst/developments
   source_rst/gallery
   source_rst/archived_version

  

  



