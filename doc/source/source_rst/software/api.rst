.. _api:


API reference
#############

.. image:: ../../../img/software/hardware_classes.png

  Schematic of the main classes architecture.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: ohmpi.ohmpi
   :members:
.. automodule:: ohmpi.hardware_system
   :members:
